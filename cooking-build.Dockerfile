FROM ubuntu:18.04
RUN apt-get update -y && apt-get install -y \ 
    curl \
    nodejs \
    npm \
    openjdk-8-jre \
    firefox wget

# RUN curl https://dl.google.com/linux/direct/google-∏-stable_current_amd64.deb -o /chrome.deb
# RUN dpkg -i /chrome.deb || apt-get install -yf
# RUN rm /chrome.deb
# RUN curl https://chromedriver.storage.googleapis.com/2.31/chromedriver_linux64.zip -o /usr/local/bin/chromedriver
# RUN chmod +x /usr/local/bin/chromedriver